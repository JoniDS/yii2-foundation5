<?php

/**
 * @author Jóni Silva <joni.duarte.silva@gmail.com>
 */

namespace jonids\yii2\foundation5\widgets;


class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $options = ['class' => 'breadcrumbs'];
    public $activeItemTemplate = "<li class=\"current\">{link}</li>\n";
    
}