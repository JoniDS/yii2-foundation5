<?php

/**
 * @author Jóni Silva <joni.duarte.silva@gmail.com>
 */

namespace jonids\yii2\foundation5\widgets;



class ActiveForm extends \yii\widgets\ActiveForm
{  
    
    public $errorCssClass = 'error';
    /**
     * Generates a form field.
     * A form field is associated with a model and an attribute. It contains a label, an input and an error message
     * and use them to interact with end users to collect their inputs for the attribute.
     * @param Model $model the data model
     * @param string $attribute the attribute name or expression. See [[Html::getAttributeName()]] for the format
     * about attribute expression.
     * @param array $options the additional configurations for the field object
     * @return ActiveField the created ActiveField object
     * @see fieldConfig
     */
    public function field($model, $attribute, $options = [])
    {
        $field = new \jonids\yii2\foundation5\widgets\ActiveField($options);
        $field->model = $model;
        $field->attribute = $attribute;
        $field->form = $this;
        return $field;
    }

}

