<?php

/*
 * Copyright 2014 jonids
 */

/**
 * Description of LinkPager
 *
 * @author jonids
 */
namespace jonids\yii2\foundation5\widgets;

class LinkPager extends \yii\widgets\LinkPager{
    
    public $activePageCssClass = 'current';
    public $disabledPageCssClass = 'unavailable';
    
}
